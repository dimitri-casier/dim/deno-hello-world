import { Application, Router } from "https://deno.land/x/oak/mod.ts";
const app = new Application({ logErrors: false });
const router = new Router();
router.get("/ws", (ctx) => {
  if (!ctx.isUpgradable) {
    ctx.throw(501);
  }
  const ws = ctx.upgrade();
  ws.onopen = () => {
    console.log("Connected to client");
    ws.send("Hello from server!");
  };
  ws.onmessage = (m) => {
    const data  = JSON.parse(m.data)
    console.log("Got message from client: ", data.type);
    ws.send(JSON.stringify(data));
    //ws.close();
  };
  ws.onclose = () => console.log("Disconncted from client");
});
app.use(router.routes());
app.use(router.allowedMethods());
app.listen({ port: 8000 });